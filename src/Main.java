
import controller.DatabaseController;
import controller.FileHandler;
import view.LoginPanel;

public class Main {

	public static void main(String[] args) {
		FileHandler f = new FileHandler();
		f.writePO("Hey this is my first PO", "PO_1");
		f.writeReceipt("Hello World! EIEI Test write file.", "testreciept");
		
		DatabaseController db_connect = new DatabaseController();
		new LoginPanel(db_connect);

	}
}
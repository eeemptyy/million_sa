package controller;

import java.sql.*;

public class DatabaseController {

	private static ResultSet result;
	private static PreparedStatement ps;
	
	private Connection connect = null;
	private int record;
	
	public DatabaseController(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager.getConnection("jdbc:mysql://158.108.30.177:3306/java_sa", "rt", "EMPTY");
//			connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/java_sa", "root", "");
			connect.setAutoCommit(true);
			
			if (connect != null) {
				System.out.println("Database Connected.");
			} else {
				System.out.println("Database Connect Failed.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void connection(){
		try {
			connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/java_sa", "root", "");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public PreparedStatement makePreparedStatement(String sql){
		try {
			ps = connect.prepareStatement(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ps;
	}
	
	public ResultSet executeSQL(PreparedStatement ps){
		try {
			result = ps.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public int executeUpdata(String sql){
		try {
			record = ps.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return record;
	}
	
	public void closeDB() {
		try {
			if (connect != null) {
				connect.close();
				System.out.println("Database Closed.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
}
package controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JTable;

import model.Customer;
import model.User;

public class FileHandler {

	private File filePath;
	private String content = "";
	private FileWriter fw;
	private BufferedWriter bf;
	private File folderPath;

	public FileHandler() {
		System.out.println("Filehandler opened.");
	}
	
	public String genDataPO(JTable table, User user, Customer customer, String date, String time){
		String username = concatSpace(user.getName(),20);
		String customername = concatSpace(customer.getName(), 20);
		String out = "\t   Million Steak Bar\t\n"+
					 "\t     Purchase Order\n"+
					 "  Date:"+date+"    Time:"+time+""+
					 "\n"+
					 "  ======== Menu ======= == Amount ==\n\n"				 
				;
		for (int i = 1; i<table.getRowCount()-1; i++){
			String menu = concatSpace(table.getValueAt(i, 0).toString(),22);
			String amount = concatSpace(table.getValueAt(i, 1).toString(),4);
			out += "  "+i+"."+menu+"  "+amount+"  \n";
			System.out.println();
		}
//		String total = concatSpace(table.getValueAt(table.getRowCount()-1, 2).toString(), 7);
		out += "\n"+
			   "  ==================================\n\n"+
			   "   cashier name       customer name \n"+
			   "    "+username+" "+customername+"\n"
				;
		return out;
	}

	public String genDataReceipt(ArrayList<ArrayList> table, User user, Customer customer, String date, String time){
		String username = concatSpace(user.getName(),20);
		String customername = concatSpace(customer.getName(), 20);
		String out = "\t\tMillion Steak Bar\t\t\n"+
					 "\t\t     Receipt\t\t\t\n"+
					 "       	Date:"+date+"         Time:"+time+""+
					 "\n"+
					 "  ======== Menu ======= == Amount == === Price ==\n\n"				 
				;
		int total = 0;
		for (int i = 1; i<table.size()-1; i++){
			String menu = concatSpace(table.get(i).get(0).toString(),22);
			String amount = concatSpace(table.get(i).get(1).toString(),4);
			String price = concatSpace(table.get(i).get(2).toString(), 7);
			out += "  "+i+"."+menu+"  "+amount+"        "+price+"  \n";
			total += Integer.parseInt(amount)*Integer.parseInt(price);
		}
//		String total = concatSpace(table.get(table.size()-1).get(2).toString(), 7);
		out += "                           Total        "+total+"\n"+
			   "\n"+
			   "  ===============================================\n\n"+
			   "    cashier name               customer name \n"+
			   "    "+username+"       "+customername+"\n"
				;
		return out;
	}
	
	public String concatSpace(String temp,int n){
		if (temp.length()<n){
			for (int i = temp.length();i<n;i++){
				temp+=" ";
			}
		}
		return temp;
	}
	
	public void writePO(String data, String filename){
		System.out.println(">>> Writing PO Files.");
		try {
			content = data.replaceAll("(?!\\r)\\n", "\r\n");
			System.out.println("Get content>: "+content);
			folderPath = new File("D:/MillionSteakBar/PO/");
			filePath = new File("D:/MillionSteakBar/PO/" + filename + ".txt");

			if (!folderPath.exists()) {
				System.out.println("Folder doesn't exists: creating new Folder");
				folderPath.mkdirs();
			}
			int i = 1;
			while (filePath.exists()){
				System.out.print("File:"+filename+" is duplicate. Creating File:");
				filename += "("+i+")";
				System.out.println(filename+" instead.");
				filePath = new File("D:/MillionSteakBar/PO/" + filename + ".txt");
				i++;
			}
			if (!filePath.exists()){
				System.out.println("File doesn't exists: creating new File:"+filename+".txt");
				filePath.createNewFile();
			}else {
				System.out.println("Create file:"+filename+" complete.");
			}
			fw = new FileWriter(filePath.getAbsolutePath());
			bf = new BufferedWriter(fw);
			bf.write(content);
			bf.close();
			
			System.out.println(">>> Writing PO Complete.");
		}

		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void writeReceipt(String data, String filename) {
		System.out.println(">>> Writing Receipt File");
		try {
			content = data;
			System.out.println("Get content>: "+content);
			folderPath = new File("D:/MillionSteakBar/Receipt/");
			filePath = new File("D:/MillionSteakBar/Receipt/" + filename + ".txt");

			if (!folderPath.exists()) {
				System.out.println("Folder doesn't exists: creating new Folder");
				folderPath.mkdirs();
			}
			int i = 1;
			while (filePath.exists()){
				System.out.print("File:"+filename+" is duplicate. Creating File:");
				filename += "("+i+")";
				System.out.println(filename+" instead.");
				filePath = new File("D:/MillionSteakBar/PO/" + filename + ".txt");
				i++;
			}
			if (!filePath.exists()){
				System.out.println("File doesn't exists: creating new File:"+filename+".txt");
				filePath.createNewFile();
			}else {
				System.out.println("Create file:"+filename+" complete.");
			}
			fw = new FileWriter(filePath.getAbsolutePath());
			bf = new BufferedWriter(fw);
			bf.write(content);
			bf.close();
			
			System.out.println(">>> Writing Receipt Complete.");
		}

		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

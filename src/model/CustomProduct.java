package model;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CustomProduct extends JPanel {

	private Image pic = null;
	private String name = "";
	private int price = 0;
	private int num = 0;
	private int amountFood = 0;

	public CustomProduct(Image pic, String name, int price, int amount) {
		this.pic = pic;
		this.name = name;
		this.price = price;
		this.amountFood = amount;
		init();

	}

	public void init() {
		setLayout(new BorderLayout());

		JLabel lb_name = new JLabel(name);
		lb_name.setVerticalAlignment(JLabel.CENTER);
		lb_name.setHorizontalAlignment(JLabel.CENTER);

		JLabel lb = new JLabel();
		lb.setVerticalAlignment(JLabel.CENTER);
		lb.setHorizontalAlignment(JLabel.CENTER);
		lb.setIcon(new ImageIcon(pic,this.name));

		JLabel lb_price = new JLabel(this.price + " ฿");
		lb_price.setVerticalAlignment(JLabel.CENTER);
		lb_price.setHorizontalAlignment(JLabel.CENTER);

		JPanel panel1 = new JPanel();
		panel1.setLayout(new BorderLayout());
		panel1.add(lb, BorderLayout.CENTER);
		panel1.add(lb_name, BorderLayout.NORTH);
		panel1.add(lb_price, BorderLayout.SOUTH);

		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
		JButton btn_add = new JButton("+");
		btn_add.setSize(15, 15);
		JButton btn_clear = new JButton("-");
		btn_clear.setSize(15, 15);

		JTextField amount = new JTextField(num + "", 3);
		amount.setEditable(false);

		btn_add.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (amountFood <= 0) {
					amount.setForeground(Color.red);
					amount.setText("หมด");
				} else {
					num++;
					if (num >= amountFood) {
						num = amountFood;
					}
					amount.setForeground(Color.BLACK);
					amount.setText(num + "");
				}
			}
		});
		btn_clear.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (amountFood <= 0) {
					amount.setForeground(Color.red);
					amount.setText("หมด");
				} else {
					num--;
					if (num <= 0) {
						num = 0;
					}
					amount.setForeground(Color.BLACK);
					amount.setText(num + "");
				}
			}
		});
		panel3.add(btn_add);
		panel3.add(btn_clear);
		panel2.add(amount);
		panel2.add(panel3);
		// JPanel panel4 = new JPanel();
		// panel4.setLayout(new BorderLayout());
		// panel4.add(panel2, BorderLayout.CENTER);
		add(panel1, BorderLayout.CENTER);
		add(panel2, BorderLayout.SOUTH);
		if (amountFood <= 0) {
			amount.setForeground(Color.red);
			amount.setText("หมด");
		}

	}

	public String getItemName() {
		return this.name;
	}

	public int getItemAmount() {
		return this.num;
	}

	public int getItemPrice() {
		return this.price;
	}

}

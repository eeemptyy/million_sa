package model;

public class Customer {

	private int id = 0;
	private String name = "";
	private String tel = "";
	private String email = "";
	
	public Customer(String name, String tel){
		this.id = 0;
		this.name = name;
		this.tel = tel;
		this.email = "";
	}
	public Customer(String name, String tel, String email){
		this.id = 0;
		this.name = name;
		this.tel = tel;
		this.email = email;
	}
	
	public String toString(){
		return "ID: " + id + "\n" +
				"Name: " + name + "\n" +
				"Tel: " + tel + "\n" +
				"E-mail: " + email + "\n";
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
}

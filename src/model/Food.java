package model;

public class Food {
	private int id;
	private String name;
	private int price;
	private int amount;
	private String picPath;
	private String course;
	
	public Food(int id, String name, int price, int amount, String picPath, String course){
		this.id = id;
		this.name = name;
		this.price = price;
		this.amount = amount;
		this.picPath = picPath;
		this.course = course;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}
	
	
}

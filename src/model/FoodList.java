package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import controller.DatabaseController;

public class FoodList {
	
	private DatabaseController db_connect;
	private ArrayList<Food> foodList = new ArrayList<>();

	public FoodList(DatabaseController db_connect){
		this.db_connect = db_connect;
	}
	public ArrayList<Food> createFoodList(){
		String sql = "select * from foodlist";
		PreparedStatement ps = db_connect.makePreparedStatement(sql);
		ResultSet result = db_connect.executeSQL(ps);
		try {
			while (result.next()){
				int id = result.getInt("id");
				String name = result.getString("name");
				int price = result.getInt("price");
				int amount = result.getInt("amount");
				String picPath = result.getString("picpath");
				String course = result.getString("course");
				foodList.add(new Food(id, name, price, amount, picPath, course));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return foodList;
	}

}

package model;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

public class FoodProduct extends JPanel {

	private Food food;
	private JCheckBox checkBox;
	private JTextField price;
	private boolean isChecked;
	
	public FoodProduct(Food food) {
		this.food = food;
		init();
	}

	public void init() {
		setLayout(new BorderLayout());
		setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		JLabel lb_name = new JLabel(food.getName());
		lb_name.setVerticalAlignment(JLabel.CENTER);
		lb_name.setHorizontalAlignment(JLabel.CENTER);

		JLabel lb = new JLabel();
		lb.setVerticalAlignment(JLabel.CENTER);
		lb.setHorizontalAlignment(JLabel.CENTER);
		Image nIcon;
		try {
			nIcon = ImageIO.read(getClass().getResource("/images/"+food.getPicPath()+".jpg"));
			lb.setIcon(new ImageIcon(nIcon,food.getName()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		lb.setIcon(new ImageIcon("image_re/"+food.getPicPath()+".jpg"));
//		lb.setIcon(new ImageIcon(getResourceAsStream(food.getPicPath()));
//		lb.setIcon(new ImageIcon());

		JPanel panel1 = new JPanel();
		panel1.setLayout(new BorderLayout());
		panel1.add(lb, BorderLayout.CENTER);
		panel1.add(lb_name, BorderLayout.NORTH);

		JPanel panel2 = new JPanel();
//		panel2.setLayout(new BorderLayout());
		
		checkBox = new JCheckBox();
		checkBox.addItemListener(new checkBoxListener());
		price = new JTextField(food.getPrice() + "", 3);
		price.setEditable(false);
		panel2.add(checkBox);
		panel2.add(price);
		add(panel1, BorderLayout.CENTER);
		add(panel2, BorderLayout.SOUTH);

	}
	
	private class checkBoxListener implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent e) {
			// TODO Auto-generated method stub
			if (checkBox.isSelected()){
//				price.setEditable(true);
				isChecked = true;
			}else {
//				price.setEditable(false);
				isChecked = false;
			}
		}
		
	}
	

	public JCheckBox getCheckBox() {
		return checkBox;
	}

	public void setCheckBox(JCheckBox checkBox) {
		this.checkBox = checkBox;
	}

	public JTextField getPrice() {
		return price;
	}

	public void setPrice(JTextField price) {
		this.price = price;
	}

	public Food getFood() {
		return food;
	}

	public void setFood(Food food) {
		this.food = food;
	}

	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}

}

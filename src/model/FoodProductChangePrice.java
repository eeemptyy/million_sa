package model;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;


public class FoodProductChangePrice extends JPanel {

	private Food food;
	private JCheckBox checkBox;
	private JTextField price;
	
	public FoodProductChangePrice(Food food) {
		this.food = food;
		init();
	}
	
	public void init() {
		setLayout(new BorderLayout());
		setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		JLabel lb_name = new JLabel(food.getName());
		lb_name.setVerticalAlignment(JLabel.CENTER);
		lb_name.setHorizontalAlignment(JLabel.CENTER);

		JLabel lb = new JLabel();
		lb.setVerticalAlignment(JLabel.CENTER);
		lb.setHorizontalAlignment(JLabel.CENTER);
		Image nIcon;
		try {
			nIcon = ImageIO.read(getClass().getResource("/images/"+food.getPicPath()+".jpg"));
			lb.setIcon(new ImageIcon(nIcon,food.getName()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JPanel panel1 = new JPanel();
		panel1.setLayout(new BorderLayout());
		panel1.add(lb, BorderLayout.CENTER);
		panel1.add(lb_name, BorderLayout.NORTH);

		JPanel panel2 = new JPanel();
		price = new JTextField(food.getPrice() + "", 3);
		price.setEditable(true);
		panel2.add(price);
		add(panel1, BorderLayout.CENTER);
		add(panel2, BorderLayout.SOUTH);

	}

	public Food getFood() {
		return food;
	}

	public void setFood(Food food) {
		this.food = food;
	}

	public JCheckBox getCheckBox() {
		return checkBox;
	}

	public void setCheckBox(JCheckBox checkBox) {
		this.checkBox = checkBox;
	}

	public JTextField getPrice() {
		return price;
	}

	public void setPrice(JTextField price) {
		this.price = price;
	}

}

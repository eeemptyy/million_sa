package model;

public class Menu {
	
	private int id;
	private int food_id;
	private int price;
	
	public Menu(int id, int food_id, int price) {	
		this.id = id;
		this.food_id = food_id;
		this.price = price;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFood_id() {
		return food_id;
	}

	public void setFood_id(int food_id) {
		this.food_id = food_id;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}

package model;

public class Receipt {

	private int id;
	private int user_id;
	private int customer_id;
	private int orderlist_id;
	private int amount;
	private int total;
	private String date;
	private String time;
	
	public Receipt(int id, int user_id, int customer_id, int orderlist_id, int amount, int total, String date, String time){
		this.id = id;
		this.user_id = user_id;
		this.customer_id = customer_id;
		this.orderlist_id = orderlist_id;
		this.amount = amount;
		this.total = total;
		this.date = date;
		this.time = time;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	public int getOrderlist_id() {
		return orderlist_id;
	}
	public void setOrderlist_id(int orderlist_id) {
		this.orderlist_id = orderlist_id;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}	
	
}
package model;

public class User {

	private int id = 0;
	private String username = "";
	private String password = "";
	private String name = "";
	private String tel = "";
	private String email = "";
	private String position = "";

	public User(String username, String password, String name, String tel, String email, String position) {
		this.username = username;
		this.password = password;
		this.name = name;
		this.tel = tel;
		this.email = email;
		this.position = position;
		
	}
	
	public String toString(){
		return 	"Username: " + username + "\n" +
				"Name: " + name + "\n" +
				"Tel: " + tel + "\n" +
				"E-mail: " + email + "\n" +
				"Position: " + position	;
	}

	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPosition() {
		return position;
	}


	public void setPosition(String position) {
		this.position = position;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return name;
	}

	public void setFirstname(String firstname) {
		this.name = firstname;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

}

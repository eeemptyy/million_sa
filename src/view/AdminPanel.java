package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import controller.DatabaseController;
import model.User;

public class AdminPanel {

	private JFrame frame;
	private boolean isOpen = false;
	private User user;
	private DatabaseController db_connect;
	private JButton btn_Summary;
	private JButton btn_Logout;
	private JButton btn_addFoodList;

	public AdminPanel(User user) {
		this.user = user;
		this.db_connect = new DatabaseController();
		init();
	}

	public AdminPanel(boolean status, User user) {
		this.isOpen = status;
		this.user = user;
		this.db_connect = new DatabaseController();
		init();
	}

	private void init() {
		frame = new JFrame();
		frame.setTitle("Million Steak Bar: AdminPanel");
		frame.setResizable(false);
		frame.setBounds(100, 100, 200, 360);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lb_ShopStatus = new JLabel("SHOP Status: ");
		lb_ShopStatus.setBounds(10, 11, 83, 25);
		frame.getContentPane().add(lb_ShopStatus);

		JLabel lb_Opened = new JLabel("Opened");
		lb_Opened.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lb_Opened.setForeground(Color.GREEN);
		lb_Opened.setBounds(88, 12, 70, 20);
		frame.getContentPane().add(lb_Opened);

		JLabel lb_Closed = new JLabel("Closed");
		lb_Closed.setForeground(Color.RED);
		lb_Closed.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lb_Closed.setBounds(88, 11, 70, 20);
		frame.getContentPane().add(lb_Closed);

		JLabel lb_AdminName = new JLabel("Admin name: ");
		lb_AdminName.setBounds(10, 47, 100, 14);
		frame.getContentPane().add(lb_AdminName);

		JLabel lb_NameAsdNaem = new JLabel(user.getName());
		lb_NameAsdNaem.setBounds(95, 47, 119, 14);
		frame.getContentPane().add(lb_NameAsdNaem);

		JButton btn_open_close = new JButton("เปิด/ปิด ร้าน");
		btn_open_close.setBounds(23, 84, 155, 35);
		btn_open_close.addActionListener(new ActionListener() {
			// UPDATE `status` SET `status` = 'true' WHERE `status`.`id` = 1
			@Override
			public void actionPerformed(ActionEvent e) {
				String tempstatus = "";
				if (isOpen) {
					int reply = JOptionPane.showConfirmDialog(null, "ต้องการปิดร้าน ?", "Million Steak Bar: Logout?",
							JOptionPane.YES_NO_OPTION);
					if (reply == JOptionPane.YES_OPTION) {
						lb_Opened.setVisible(!isOpen);
						lb_Closed.setVisible(isOpen);
						// btn_addFoodList.setEnabled(!isOpen);
						// btn_Summary.setEnabled(isOpen);
						isOpen = !isOpen;
//						String sql = "update status set status = '" + isOpen + "' where status.id = 1";
//						db_connect.executeUpdata(sql);
					}
				} else {
					lb_Opened.setVisible(!isOpen);
					lb_Closed.setVisible(isOpen);
					// btn_addFoodList.setEnabled(!isOpen);
					// btn_Summary.setEnabled(isOpen);
					isOpen = !isOpen;
//					String sql = "update status set status = '" + isOpen + "' where status.id = 1";
//					db_connect.executeUpdata(sql);
				}
				if (isOpen){
					tempstatus = "t";
				}else {
					tempstatus = "f";
				}
				String sql = "update status set status = '" + tempstatus + "' where status.id = 1";
				db_connect.executeUpdata(sql);
				System.out.println("Status " + tempstatus + " has updated.");
			}
		});
		frame.getContentPane().add(btn_open_close);
		
		JButton btn_editFoodList = new JButton("สร้างรายการอาหาร");
		btn_editFoodList.setBounds(23, 130, 155, 35);
		btn_editFoodList.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (isOpen) {
					new CreateFoodPanel();
				} else {
					JOptionPane.showMessageDialog(null, "กรุณาเปิดร้านก่อนทำรายการ", "Million Steak Bar:Error massage",
							JOptionPane.ERROR_MESSAGE);
				}
				
			}
		});
		frame.getContentPane().add(btn_editFoodList);
		
		btn_addFoodList = new JButton("เพิ่มเมนูอาหาร");
		btn_addFoodList.setBounds(23, 176, 155, 35);
		frame.getContentPane().add(btn_addFoodList);
		btn_addFoodList.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (isOpen) {
					new FoodListPanel(isOpen, user, db_connect);
				} else {
					JOptionPane.showMessageDialog(null, "กรุณาเปิดร้านก่อนทำรายการ", "Million Steak Bar:Error massage",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		btn_Summary = new JButton("สรุปยอดขายประจำวัน");
		btn_Summary.setBounds(23, 222, 155, 35);
		frame.getContentPane().add(btn_Summary);
		btn_Summary.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (!isOpen) {
					new DaySummaryPanel();
					System.out.println("CHECK");
				} else {
					JOptionPane.showMessageDialog(null, "กรุณาปิดร้านก่อนทำรายการ", "Million Steak Bar:Error massage",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		btn_Logout = new JButton("LOGOUT");
		btn_Logout.setBounds(23, 268, 155, 35);
		frame.getContentPane().add(btn_Logout);
		btn_Logout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int reply = JOptionPane.showConfirmDialog(null, "Confirm to Logout ?", "Logout ?",
						JOptionPane.YES_NO_OPTION);
				if (reply == JOptionPane.YES_OPTION) {
					new LoginPanel(db_connect);
					frame.dispose();
				}
			}
		});

		lb_Opened.setVisible(isOpen);
		lb_Closed.setVisible(!isOpen);
		// btn_addFoodList.setEnabled(isOpen);
		// btn_Summary.setEnabled(!isOpen);

		frame.setVisible(true);
	}

}

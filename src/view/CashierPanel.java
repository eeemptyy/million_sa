package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.DatabaseController;
import model.Customer;
import model.User;

public class CashierPanel {

	private JFrame frame;
	private boolean isOpen = false;
	private User user;
	private DatabaseController db_connect;
	private JButton btn_OrderList;
	private JButton btn_Logout;
	private JButton btn_Menu;
	private OrderListPanel orderListpane;

	public CashierPanel(User user) {
		this.user = user;
		this.db_connect = new DatabaseController();
		orderListpane = new OrderListPanel();
		orderListpane.setVisible(true);
		init();
	}

	public CashierPanel(boolean status, User user) {
		this.isOpen = status;
		this.user = user;
		this.db_connect = new DatabaseController();
		orderListpane = new OrderListPanel();
		orderListpane.setVisible(true);
		init();
	}

	private void init() {
		frame = new JFrame();
		frame.setTitle("Million Steak Bar: CashierPanel");
		frame.setResizable(false);
		frame.setBounds(100, 100, 200, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lb_ShopStatus = new JLabel("SHOP Status: ");
		lb_ShopStatus.setBounds(10, 11, 83, 25);
		frame.getContentPane().add(lb_ShopStatus);

		JLabel lb_Opened = new JLabel("Opened");
		lb_Opened.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lb_Opened.setForeground(Color.GREEN);
		lb_Opened.setBounds(88, 12, 70, 20);
		frame.getContentPane().add(lb_Opened);

		JLabel lb_Closed = new JLabel("Closed");
		lb_Closed.setForeground(Color.RED);
		lb_Closed.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lb_Closed.setBounds(88, 11, 70, 20);
		frame.getContentPane().add(lb_Closed);

		JLabel lb_AdminName = new JLabel("Cashier name: ");
		lb_AdminName.setBounds(10, 47, 100, 14);
		frame.getContentPane().add(lb_AdminName);

		JLabel lb_NameAsdNaem = new JLabel(user.getName());
		lb_NameAsdNaem.setBounds(95, 47, 125, 14);
		frame.getContentPane().add(lb_NameAsdNaem);

		btn_Menu = new JButton("เมนูอาหาร");
		btn_Menu.setBounds(23, 84, 155, 35);
		frame.getContentPane().add(btn_Menu);
		btn_Menu.addActionListener(new ActionListener() {
			private Customer customer;

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				JTextField name_fld = new JTextField(5);
				name_fld.setText("Nut");
				JTextField tel_fld = new JTextField(5);
				tel_fld.setText("0909199218");
				JTextField email_fld = new JTextField(5);
				email_fld.setText("nuttattun@email.com");

				JPanel myPanel = new JPanel();
				myPanel.add(new JLabel("Name: "));
				myPanel.add(name_fld);
				myPanel.add(Box.createHorizontalStrut(15)); // a spacer
				myPanel.add(new JLabel("Tel: "));
				myPanel.add(tel_fld);
				myPanel.add(Box.createHorizontalStrut(15)); // a spacer
				myPanel.add(new JLabel("E-mail: "));
				myPanel.add(email_fld);
				boolean status = false;
				while (!status) {
					int result = JOptionPane.showConfirmDialog(null, myPanel, "Please Enter Customer data.",
							JOptionPane.OK_CANCEL_OPTION);
					if (result == JOptionPane.OK_OPTION) {
						String name = name_fld.getText();
						String tel = tel_fld.getText();
						String email = email_fld.getText();
						if (name.length() > 0) {
							if (tel.length() > 0) {
								if (email.length() > 0) {
									customer = new Customer(name, tel, email);
									String sql = "INSERT INTO `customer` (`id`, `name`, `tel`, `email`) VALUES (NULL, '"+name+"', '"+tel+"', '"+email+"')";
									db_connect.executeUpdata(sql);
									sql = "SELECT * FROM customer ORDER BY id DESC LIMIT 0,1";
									PreparedStatement ps = db_connect.makePreparedStatement(sql);
									ResultSet resultSet = db_connect.executeSQL(ps);
									try {
										resultSet.next();
										customer.setId(resultSet.getInt("id"));
									} catch (SQLException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
									status = true;
									System.out.println("Customer value>> "+ customer.toString());
									new MenuPanel(user, customer);
								} 
//								else {
//									customer = new Customer(name, tel);
//									status = true;
//									new MenuPanel(user, customer);
//								}
							} else {
								JOptionPane.showMessageDialog(null, "Please enter name and tel.\n", "Wrong data",
										JOptionPane.ERROR_MESSAGE);
								status = false;
							}
						} else {
							JOptionPane.showMessageDialog(null, "Please enter name.\n", "Wrong data",
									JOptionPane.ERROR_MESSAGE);
							status = false;
						}
					} else {
						status = true;
					}
				}
				// frame.dispose();
			}
		});

		btn_OrderList = new JButton("รายการใบสั่งอาหาร");
		btn_OrderList.setBounds(23, 130, 155, 35);
		frame.getContentPane().add(btn_OrderList);
		btn_OrderList.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (orderListpane.isVisible()){
					orderListpane.dispose();
				}else {
					orderListpane =new OrderListPanel();
					orderListpane.setVisible(true);
				}
			}
		});

		btn_Logout = new JButton("LOGOUT");
		btn_Logout.setBounds(23, 176, 155, 35);
		frame.getContentPane().add(btn_Logout);
		btn_Logout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int reply = JOptionPane.showConfirmDialog(null, "Confirm to Logout ?", "Logout ?",
						JOptionPane.YES_NO_OPTION);
				if (reply == JOptionPane.YES_OPTION) {
					new LoginPanel(db_connect);
					frame.dispose();
				}
				// new Logout(user, db_connect);

			}
		});

		lb_Opened.setVisible(isOpen);
		lb_Closed.setVisible(!isOpen);
		btn_Menu.setEnabled(isOpen);
		btn_OrderList.setEnabled(isOpen);

		frame.setVisible(true);
	}
}

package view;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.DatabaseController;

public class CreateFoodPanel {
	private JFrame frame;
	private DatabaseController db_connect;

	public CreateFoodPanel() {
		db_connect = new DatabaseController();
		init();
	}

	private void init() {
		frame = new JFrame();
		frame.setTitle("Million Steak Bar: AddNewFood");
		frame.setResizable(false);
		frame.setBounds(100, 100, 400, 310);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lb_AdminName = new JLabel("เพิ่มอาหารใหม่");
		lb_AdminName.setBounds(130, 5, 200, 50);
		lb_AdminName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		frame.getContentPane().add(lb_AdminName);

		JLabel lb_foodname = new JLabel("ชื่อรายการอาหาร");
		lb_foodname.setBounds(50, 60, 200, 30);
		lb_foodname.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(lb_foodname);

		JTextField tf_name = new JTextField();
		tf_name.setBounds(165, 70, 120, 20);
		frame.getContentPane().add(tf_name);

		JLabel lb_price = new JLabel("ราคา");
		lb_price.setBounds(50, 95, 200, 30);
		lb_price.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(lb_price);

		JTextField tf_price = new JTextField();
		tf_price.setBounds(165, 105, 120, 20);
		frame.getContentPane().add(tf_price);

		JLabel lb_course = new JLabel("ประเภท");
		lb_course.setBounds(50, 130, 200, 30);
		lb_course.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(lb_course);

		JTextField tf_course = new JTextField();
		tf_course.setBounds(165, 142, 120, 20);
		frame.getContentPane().add(tf_course);

		JLabel lb_picname = new JLabel("ชื่อไฟล์รูป เช่น BeefSteak240");
		lb_picname.setBounds(50, 165, 200, 30);
		lb_picname.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(lb_picname);

		JTextField tf_pic = new JTextField();
		tf_pic.setBounds(230, 177, 120, 20);
		tf_pic.setText("NoImage000");
		frame.getContentPane().add(tf_pic);

		JButton btn_accept = new JButton("เพิ่ม");
		btn_accept.setBounds(105, 240, 75, 25);
		btn_accept.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String sql = "";
				String name = tf_name.getText();
				String price = tf_price.getText();
				String course = tf_course.getText();
				String picname = tf_pic.getText();
				if (name.isEmpty() || price.isEmpty() || course.isEmpty() || picname.isEmpty()) {
					JOptionPane.showMessageDialog(null, "กรุณากรอกข้อมูลให้ครบ", "Million Steak Bar:Error massage",
							JOptionPane.ERROR_MESSAGE);

				} else {
					if (isInteger(price)) {
						sql = "INSERT INTO `foodlist` (`id`, `name`, `price`, `amount`, `picpath`, `course`) "
								+ "VALUES (NULL, '" + name + "', '" + price + "', '" + 99 + "', '" + picname
								+ "', '" + course + "')";
						db_connect.executeUpdata(sql);
						JOptionPane.showMessageDialog(null, "สร้าง Menu สำเร็จแล้ว.\n", "Complete",
								JOptionPane.INFORMATION_MESSAGE);
						frame.dispose();
					} else {
						JOptionPane.showMessageDialog(null, "ราคา และจำนวนต้องเป็นตัวเลขจำนวนเต็ม", "Million Steak Bar:Error massage",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		frame.getContentPane().add(btn_accept);

		JButton btn_cancel = new JButton("ยกเลิก");
		btn_cancel.setBounds(185, 240, 75, 25);
		btn_cancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				frame.dispose();
			}
		});

		frame.getContentPane().add(btn_cancel);
		frame.getRootPane().setDefaultButton(btn_accept);
		frame.setVisible(true);
	}

	public static boolean isInteger(String str) {
		if (str == null) {
			return false;
		}
		int length = str.length();
		if (length == 0) {
			return false;
		}
		int i = 0;
		if (str.charAt(0) == '-') {
			if (length == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < length; i++) {
			char c = str.charAt(i);
			if (c < '0' || c > '9') {
				return false;
			}
			if (c == '.'){
				return false;
			}
		}
		return true;
	}
}

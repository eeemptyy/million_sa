package view;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import controller.DatabaseController;
import model.Food;
import model.FoodProduct;
import model.FoodProductChangePrice;

public class CreateMenuPanel {

	private JFrame frame;
	private ArrayList<FoodProduct> FoodChoosen = new ArrayList<>();
	private ArrayList<FoodProduct> MainFood = new ArrayList<>();
	private ArrayList<FoodProduct> StarterFood = new ArrayList<>();
	private ArrayList<FoodProductChangePrice> FoodChanged = new ArrayList<>();

	private DatabaseController db_connect;

	public CreateMenuPanel(ArrayList<FoodProduct> choosen) {
		this.FoodChoosen = choosen;
		for (int i = 0; i < FoodChoosen.size(); i++) {
			FoodProduct temp = FoodChoosen.get(i);
			System.out.println(temp.getFood().getCourse() + "XXX");
			String tempCourse = temp.getFood().getCourse();
			if (tempCourse.equals("m")) {
				MainFood.add(temp);
			} else if (tempCourse.equals("s")) {
				StarterFood.add(temp);
			}
		}
		init();
	}

	public void init() {
		frame = new JFrame();
		frame.setTitle("Million Steak Bar: CreateMenu");
		frame.setBounds(350, 100, 650, 430);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel maincor_panel = new JPanel();
		maincor_panel.setBounds(0, 0, 624, 150);
		maincor_panel.setLayout(new GridLayout(1, 4));
		frame.getContentPane().add(maincor_panel);
		maincor_panel.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0), 1, true), "อาหารจารหลัก",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));

		for (int i = 0; i < MainFood.size(); i++) {
			Food food = MainFood.get(i).getFood();
			FoodProductChangePrice temp = new FoodProductChangePrice(food);
			FoodChanged.add(temp);
			maincor_panel.add(temp);
		}

		JPanel start_panel = new JPanel();
		start_panel.setBounds(0, 151, 624, 150);
		start_panel.setLayout(new GridLayout(1, 4));
		frame.getContentPane().add(start_panel);
		start_panel.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0), 1, true), "อาหารว่าง",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));

		for (int i = 0; i < StarterFood.size(); i++) {
			Food food = StarterFood.get(i).getFood();
			FoodProductChangePrice temp = new FoodProductChangePrice(food);
			FoodChanged.add(temp);
			start_panel.add(temp);
		}

		JLabel lb_extra = new JLabel("หมายเหตุ");
		lb_extra.setBounds(10, 307, 56, 14);
		frame.getContentPane().add(lb_extra);

		JTextArea extra_text = new JTextArea();
		extra_text.setBounds(10, 322, 614, 26);
		frame.getContentPane().add(extra_text);

		JButton btn_confirm = new JButton("ยืนยัน");
		btn_confirm.setBounds(436, 348, 89, 23);
		btn_confirm.addActionListener(new ActionListener() {
			// INSERT INTO `menu` (`id`, `food_id`, `price`) VALUES (NULL, '1',
			// '340');
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				db_connect = new DatabaseController();
				String sql = "delete from menu";
				db_connect.executeUpdata(sql);
				boolean isCorrect = true;
				FoodProductChangePrice temp;
				for (int i = 0; i < FoodChanged.size(); i++) {
					temp = FoodChanged.get(i);
					String testInt = temp.getPrice().getText();
					if (isInteger(testInt)) {
						System.out.println("Test INT: "+isInteger(testInt));
						int price = Integer.parseInt(temp.getPrice().getText());
						if (price <= 0) {
							isCorrect = false;
							break;
						}
					}else {
						isCorrect = false;
					}
				}
				if (isCorrect) {
					for (int i = 0; i < FoodChanged.size(); i++) {
						temp = FoodChanged.get(i);
						Food food = temp.getFood();
						sql = "INSERT INTO menu (`id`,`food_id`, `price`) VALUES (NULL, '"  + food.getId() + "', '"
								+ temp.getPrice().getText() + "')"; // '1','340')";
						db_connect.executeUpdata(sql);
						System.out.println("Insert Complete.");
					}
					JOptionPane.showMessageDialog(null, "สร้าง Menu สำเร็จแล้ว.\n", "Complete",
							JOptionPane.INFORMATION_MESSAGE);
					// new MenuPanel(FoodChanged);
					// new MenuPanel();
					frame.dispose();
				} else {
					JOptionPane.showMessageDialog(null, "ใส่ข้อมูลผิด ราคาต้องมากกว่า 0 บาท หรือเป็นตัวเลขเท่านั้น.\n",
							"Wrong Input data", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		frame.getContentPane().add(btn_confirm);

		JButton btn_cancle = new JButton("ยกเลิก");
		btn_cancle.setBounds(535, 348, 89, 23);
		frame.getContentPane().add(btn_cancle);
		btn_cancle.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				// new Menu();
				frame.dispose();
			}
		});

		frame.setVisible(true);

	}

	public static boolean isInteger(String str) {
		if (str == null) {
			return false;
		}
		int length = str.length();
		if (length == 0) {
			return false;
		}
		int i = 0;
		if (str.charAt(0) == '-') {
			if (length == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < length; i++) {
			char c = str.charAt(i);
			if (c < '0' || c > '9') {
				return false;
			}
			if (c == '.'){
				return false;
			}
		}
		return true;
	}

}

package view;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;

import controller.DatabaseController;
import model.Customer;
import model.Receipt;
import model.User;

public class DaySummaryPanel extends JFrame {
	private int id=0;
	private int foodOrder_id=0;
	private int totalDish = 0;
	private int totalPrice = 0;
	private String status="";
	private ResultSet resultSet=null;
	private PreparedStatement ps=null;
	private String sql="";
	private JTable table=null;
	private Object[][] rowData=null;
	private Object[] columnNames = {"ลำดับ", "ชื่อแคชเชียร์", "ชื่อลูกค้า", "เลขที่ใบสั่ง", "จำนวนจาน", "ราคารวม"};
	private DefaultTableModel tm=null;
	private Timer a=null;
	private DatabaseController db_connect= new DatabaseController();
	private JLabel lb_date, lb_time;
	private ArrayList<Receipt> repList = new ArrayList<>();
	private ArrayList<ArrayList> dataList = new ArrayList<>();
	User user = null;
	Customer customer = null;

	public DaySummaryPanel() {
		db_connect = new DatabaseController();
		init();
	}

	public void init() {
		setTitle("Million Steak Bar: Daily Summary");
		setBounds(350, 100, 550, 360);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout());

		JLabel lb_dailysummary = new JLabel("      สรุปยอดขายรายวัน Million Steak Bar");
		lb_dailysummary.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lb_dailysummary.setBounds(128, 11, 217, 47);

		lb_date = new JLabel("        DATE: "+LocalDate.now().toString());
		lb_time = new JLabel("        TIME: "+LocalTime.now().getHour()+":"+LocalTime.now().getMinute());

		JPanel topPanel = new JPanel();
		topPanel.setLayout(new GridLayout(2, 1));

		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new GridLayout(2, 1));

		JPanel t1Panel = new JPanel();
		t1Panel.setLayout(new BorderLayout());
		t1Panel.add(lb_dailysummary, BorderLayout.CENTER);

		JPanel t2Panel = new JPanel();
		t2Panel.setLayout(new GridLayout(1, 2));
		t2Panel.setBounds(0, 70, 550, 40);
		t2Panel.add(lb_date);
		t2Panel.add(lb_time);

		topPanel.add(t1Panel);
		topPanel.add(t2Panel);

		JPanel b1Panel = new JPanel(new GridLayout(1, 4));
		JPanel b2Panel = new JPanel(new GridLayout(1, 5));

		JLabel lb_printName = new JLabel("พิมพ์โดย empty");
		b1Panel.add(lb_printName);
		b1Panel.add(new JPanel());
		b1Panel.add(new JPanel());
		b1Panel.add(new JPanel());

		JButton btn_print = new JButton("พิมพ์ใบสรุปยอดขายประจำวัน");
		b2Panel.add(new JPanel());
		b2Panel.add(new JPanel());
		b2Panel.add(new JPanel());
		b2Panel.add(btn_print);
		b2Panel.add(new JPanel());

		bottomPanel.add(b1Panel);
		bottomPanel.add(b2Panel);

		queryReceipt(LocalDate.now()+"");
//		queryReceipt(LocalDate.now().toString());
		rowData = getRowData();
//		Object tempData[][] = {{"2","Ploy","Nut","4","2","200"},{"4","Ploy","M","7","2","720"}};
		// columnNames = {"ลำดับ", "ชื่อแคชเชียร์", "ชื่อลูกค้า", "เลขที่ใบสั่ง", "จำนวนจาน", "ราคารวม"};
		tm = new DefaultTableModel(rowData, columnNames);
		table = new JTable(tm);
		JScrollPane scrollPane = new JScrollPane(table);

		JTextField orderId = new JTextField(5);
		orderId.setEditable(true);

		add(scrollPane, BorderLayout.CENTER);
		add(topPanel, BorderLayout.NORTH);
		add(bottomPanel, BorderLayout.SOUTH);
		setVisible(true);
	}

	public Object[][] getRowData() {
		dataList = new ArrayList<>();
		Object[][] rowdata = null;
		for (int i = 0; i < repList.size(); i++) {
			Receipt tempRep = repList.get(i);
			db_connect = new DatabaseController();
			sql = "SELECT * FROM `orders` WHERE orders.id = '" + tempRep.getOrderlist_id() + "'";
			ps = db_connect.makePreparedStatement(sql);
			resultSet = db_connect.executeSQL(ps);
			
			try {
				System.out.println("In try Day Sum");
				resultSet.next();
				int amountDish = resultSet.getInt("amount");
				int totalPrice = resultSet.getInt("total");
				user = new User(resultSet.getString("username"), resultSet.getString("password"),
						resultSet.getString("name"), resultSet.getString("tel"), resultSet.getString("email"),
						resultSet.getString("position"));
				db_connect = new DatabaseController();
				sql = "SELECT * FROM `customer` WHERE id = '" + resultSet.getInt("customer_id") + "'";
				ps = db_connect.makePreparedStatement(sql);
				resultSet = db_connect.executeSQL(ps);
				resultSet.next();
				customer = new Customer(resultSet.getString("name"), resultSet.getString("tel"), resultSet.getString("email"));
				ArrayList<String> temp = new ArrayList<>();
				temp.add(tempRep.getId()+"");
				temp.add(user.getName());
				temp.add(customer.getName());
				temp.add(tempRep.getOrderlist_id()+"");
				temp.add(amountDish+"");
				temp.add(totalPrice+"");
				dataList.add(temp);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		rowdata = new Object[dataList.size()+1][6];
		for (int i = 0; i<dataList.size();i++){
			ArrayList<String> temp = dataList.get(i);
			rowdata[i][0] = temp.get(0);
			rowdata[i][1] = temp.get(1);
			rowdata[i][2] = temp.get(2) ;
			rowdata[i][3] = temp.get(3);
			rowdata[i][4] = temp.get(4); // Amount of dish per Rep
			rowdata[i][5] = temp.get(5); // Total price of dish per Rep
			totalDish += Integer.parseInt(temp.get(4));
			totalPrice += Integer.parseInt(temp.get(5));
		}
		rowdata[dataList.size()][0] = "";
		rowdata[dataList.size()][1] = "";
		rowdata[dataList.size()][2] = "";
		rowdata[dataList.size()][3] = "Total";
		rowdata[dataList.size()][4] = totalDish+"";
		rowdata[dataList.size()][5] = totalPrice+"";

		return rowdata;
	}

	public void queryReceipt(String dateIn) {
		repList = new ArrayList<>();
		db_connect = new DatabaseController();
		sql = "SELECT * FROM `receipt` WHERE date = '" + dateIn + "'";
		ps = db_connect.makePreparedStatement(sql);
		resultSet = db_connect.executeSQL(ps);
		try {
			System.out.println("In try of queryRep");
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				int user_id = resultSet.getInt("user_id");
				int customer_id = resultSet.getInt("customer_id");
				int orderlist_id = resultSet.getInt("orderlist_id");
				String date = resultSet.getString("date");
				String time = resultSet.getString("time");
				int amount = resultSet.getInt("amount");
				int total = resultSet.getInt("total");
				repList.add(new Receipt(id, user_id, customer_id, orderlist_id, amount, total, date, time));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getTotalPrice(int foodOr_id) {
		System.out.println("INNN: " + foodOr_id);
		int total = 0;
		DatabaseController db_connect = new DatabaseController();
		String sql = "SELECT * FROM `food_order` WHERE id = '" + foodOr_id + "'";
		PreparedStatement ps = db_connect.makePreparedStatement(sql);
		ResultSet resultSet = db_connect.executeSQL(ps);
		ArrayList<Integer> menuIdList = new ArrayList<>();
		ArrayList<Integer> amountList = new ArrayList<>();
		ArrayList<Integer> priceList = new ArrayList<>();
		try {
			while (resultSet.next()) {
				int memid = resultSet.getInt("menu_id");
				int amo = resultSet.getInt("amount");
				menuIdList.add(memid);
				amountList.add(amo);
				System.out.println("Memid: " + memid + " |amo: " + amo);
			}
			for (int i = 0; i < menuIdList.size(); i++) {
				sql = "SELECT * FROM `menu` WHERE id = '" + menuIdList.get(i) + "'";
				ps = db_connect.makePreparedStatement(sql);
				resultSet = db_connect.executeSQL(ps);
				while (resultSet.next()) {
					int pri = resultSet.getInt("price");
					priceList.add(pri);
					System.out.println("pri: " + pri);
				}
			}
			for (int i = 0; i < priceList.size(); i++) {
				total += (priceList.get(i) * amountList.get(i));
				System.out.println("Total " + total);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return total;
	}
}

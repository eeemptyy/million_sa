package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import controller.DatabaseController;
import model.Food;
import model.FoodList;
import model.FoodProduct;
import model.User;

public class FoodListPanel {

	private JFrame frame;
	private boolean isOpen;
	private User user;
	private DatabaseController db_connect;
	private ArrayList<Food> foodList;
	private ArrayList<FoodProduct> FoodPanelList = new ArrayList<>();
	private ArrayList<FoodProduct> FoodChoosen = new ArrayList<>();
	private FoodList foodDb;

	public FoodListPanel(boolean status, User user, DatabaseController db_connect) {
		this.isOpen = status;
		this.user = user;
		this.db_connect = db_connect;
		foodDb = new FoodList(db_connect);
		foodList = foodDb.createFoodList();
//		createFoodList();
		init();
	}
	
	public void createFoodList(){
		String sql = "select * from foodlist";
		PreparedStatement ps = db_connect.makePreparedStatement(sql);
		ResultSet result = db_connect.executeSQL(ps);
		try {
			while (result.next()){
				int id = result.getInt("id");
				String name = result.getString("name");
				int price = result.getInt("price");
				int amount = result.getInt("amount");
				String picPath = result.getString("picpath");
				String course = result.getString("course");
				foodList.add(new Food(id, name, price, amount, picPath, course));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void init() {
		frame = new JFrame();
		frame.setTitle("Million Steak Bar: FoodList");
		frame.setBounds(350, 100, 650, 430);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setLayout(new BorderLayout());

		JPanel panel1 = new JPanel();
		panel1.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0), 1, true), "กรุณาเลือกอาหาร",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel1.setLayout(new GridLayout(3, 3));
		for (int i = 0; i < foodList.size(); i++) {
			FoodPanelList.add(new FoodProduct(foodList.get(i)));
		}

		for (int i = 0; i < FoodPanelList.size(); i++) {
			panel1.add(FoodPanelList.get(i));
		}

		JPanel panel2 = new JPanel();

		JButton btn_confirm = new JButton("ยืนยัน");
		btn_confirm.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				for (int i = 0; i < FoodPanelList.size(); i++) {
					FoodProduct temp = FoodPanelList.get(i);
					if (temp.isChecked()) {
						System.out.println("Is Check");
						FoodChoosen.add(temp);
					}
				}
				if (!FoodChoosen.isEmpty()) {
					new CreateMenuPanel(FoodChoosen);
					frame.dispose();
				} else {
					JOptionPane.showMessageDialog(null, "กรุณาเลือกรายการอาหาร",
							"Million Steak Bar:Error massage", JOptionPane.ERROR_MESSAGE);
				}

			}
		});

		JButton btn_cancle = new JButton("ยกเลิก");
		btn_cancle.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				frame.dispose();
			}
		});

		panel2.add(btn_confirm);
		panel2.add(btn_cancle);
		frame.add(panel1, BorderLayout.CENTER);
		frame.add(panel2, BorderLayout.SOUTH);
		frame.setVisible(true);
	}

}

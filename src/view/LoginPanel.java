package view;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import controller.DatabaseController;
import model.User;

public class LoginPanel extends JFrame {

	private JTextField email_fld;
	private JPasswordField password_fld;
	private DatabaseController db_connect;
	private boolean isOpen;
	private String forced = "admin";
	private String forced2 = "cash";
	
	public LoginPanel(DatabaseController db_connect) {
		this.db_connect = db_connect;
		try {
			getStatus();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		init();
	}

	public void init() {

		setTitle("Million Steak Bar: LOGIN");
		setBounds(500, 200, 500, 360);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);

		JLabel lb_MillionSteakBar = new JLabel("Million Steak Bar");
		lb_MillionSteakBar.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lb_MillionSteakBar.setBounds(132, 73, 221, 31);
		getContentPane().add(lb_MillionSteakBar);

		JLabel lb_email = new JLabel("E-mail");
		lb_email.setBounds(133, 126, 46, 14);
		getContentPane().add(lb_email);

		email_fld = new JTextField();
		email_fld.setText(forced);
		email_fld.setBounds(189, 123, 160, 20);
		getContentPane().add(email_fld);
		email_fld.setColumns(50);

		JLabel lb_password = new JLabel("Password");
		lb_password.setBounds(133, 154, 46, 14);
		getContentPane().add(lb_password);

		password_fld = new JPasswordField();
		password_fld.setEchoChar('*');
		password_fld.setText(forced);
		password_fld.setBounds(189, 151, 160, 20);
		getContentPane().add(password_fld);
		password_fld.setColumns(20);

		JButton btnLogin = new JButton("LOGIN");
		btnLogin.setBounds(199, 182, 89, 23);
		getContentPane().add(btnLogin);
		btnLogin.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String sql = "select * from user";
				PreparedStatement ps = db_connect.makePreparedStatement(sql);
				ResultSet result = db_connect.executeSQL(ps);
				String name = email_fld.getText();
				char[] pass = password_fld.getPassword();
				boolean checker = false;
				User user;
				try {
					while (result.next()) {
						checker = false;
						String username = result.getString("username");
						char[] password = result.getString("password").toCharArray();
						String position = result.getString("position");
						if (name.equals(username)) {
							if (password.length == pass.length) {
								for (int i = 0; i < name.length(); i++) {
									if (pass[i] != password[i]) {
										checker = false;
										break;
									} else {
										checker = true;
									}
								}
							}
						}
						if (checker && (position.equals("m"))) {
							user = new User(result.getString("username"),
									result.getString("password"), result.getString("name"), result.getString("tel"),
									result.getString("email"), result.getString("position"));
							new AdminPanel(isOpen, user);
							dispose();
							break;
						} else if (checker && (position.equals("c"))) {
							user = new User(result.getString("username"),
									result.getString("password"), result.getString("name"), result.getString("tel"),
									result.getString("email"), result.getString("position"));
							new CashierPanel(isOpen, user);
							dispose();
							break;
						}
					}
					if (!checker) {
						if (name.isEmpty()) {
							JOptionPane.showMessageDialog(null, "Please enter username or password.\n", "Can't login",
									JOptionPane.ERROR_MESSAGE);
						} else {
							JOptionPane.showMessageDialog(null, "Wrong username or password.\n" + "Please login again.",
									"Can't login", JOptionPane.ERROR_MESSAGE);
						}
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		getRootPane().setDefaultButton(btnLogin);
		setVisible(true);
	}

	public void getStatus() throws SQLException {
		String sql = "select * from status";
//		String sql = "select * from status";
		PreparedStatement ps = db_connect.makePreparedStatement(sql);
		ResultSet result = db_connect.executeSQL(ps);
		result.next();
		String temp = result.getString("status");
		System.out.println("Status new: > " + temp);
		if (temp.equals("t")){
			temp = "true";
		}else {
			temp = "false";
		}
		System.out.println("Status new: > " + temp);
		isOpen = Boolean.parseBoolean(temp);
	}

}

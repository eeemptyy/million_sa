package view;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import controller.DatabaseController;
import model.CustomProduct;
import model.Customer;
import model.Food;
import model.Menu;
import model.User;

public class MenuPanel {

	private JFrame frame;
	private ArrayList<Food> FoodChoosen = new ArrayList<>();
	private ArrayList<Food> MainFood = new ArrayList<>();
	private ArrayList<Food> StarterFood = new ArrayList<>();
	private ArrayList<CustomProduct> foodList = new ArrayList<>();
	private ArrayList<Menu> MenuList = new ArrayList<>();
	private DatabaseController db_connect;
	private User user = null;
	private Customer customer = null;

	public MenuPanel(User user, Customer customer) {
		this.user = user;
		this.customer = customer;
		db_connect = new DatabaseController();
		String sql = "select * from menu";
		PreparedStatement ps = db_connect.makePreparedStatement(sql);
		ResultSet result = db_connect.executeSQL(ps);
		try {
			while (result.next()) {
				int menuID = result.getInt("id");
				int food_id = result.getInt("food_id");
				int price = result.getInt("price");
				MenuList.add(new Menu(menuID, food_id, price));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		genFoodChoosen();

		for (int i = 0; i < FoodChoosen.size(); i++) {
			Food temp = FoodChoosen.get(i);
			String tempCourse = temp.getCourse();
			System.out.println("Course:> " + tempCourse + " :< " + temp.getName());
			if (tempCourse.equals("m")) {
				MainFood.add(temp);
			} else if (tempCourse.equals("s")) {
				StarterFood.add(temp);
			}
		}

		init();
	}

	public void genFoodChoosen() {
		for (int i = 0; i < MenuList.size(); i++) {
			Menu menu = MenuList.get(i);
			String sql = "select * from foodlist where id = '" + menu.getFood_id() + "'";
			PreparedStatement ps = db_connect.makePreparedStatement(sql);
			ResultSet result = db_connect.executeSQL(ps);
			try {
				while (result.next()) {
					int id = result.getInt("id");
					String name = result.getString("name");
					int price = menu.getPrice();
					int amount = result.getInt("amount");
					String picPath = result.getString("picpath");
					String course = result.getString("course");
					this.FoodChoosen.add(new Food(id, name, price, amount, picPath, course));
					System.out.println(result.getString("name") + " was generated.");
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void init() {
		frame = new JFrame();
		frame.setTitle("Million Steak Bar: MenuPanel");
		frame.setBounds(350, 100, 650, 460);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lb_uname = new JLabel("Cashier name: " + user.getFirstname());
		lb_uname.setBounds(10, 10, 180, 25);
		JLabel lb_cusname = new JLabel("Customer name: " + customer.getName());
		lb_cusname.setBounds(250, 10, 180, 25);
		JLabel lb_cusTel = new JLabel("Tel: " + customer.getTel());
		lb_cusTel.setBounds(450, 10, 180, 25);
		frame.add(lb_uname);
		frame.add(lb_cusname);
		frame.add(lb_cusTel);
		JPanel maincor_panel = new JPanel();
		maincor_panel.setBounds(0, 35, 624, 150);
		maincor_panel.setLayout(new GridLayout(1, 4));
		frame.getContentPane().add(maincor_panel);
		maincor_panel.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0), 1, true), "อาหารจารหลัก",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		Image nIcon = null;
		for (int i = 0; i < MainFood.size(); i++) {
			Food food = MainFood.get(i);
			int priceNew = food.getPrice();
			try {
				nIcon = ImageIO.read(getClass().getResource("/images/"+food.getPicPath()+".jpg"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			CustomProduct temp = new CustomProduct(nIcon, food.getName(), priceNew,
					food.getAmount());
//			CustomProduct temp = new CustomProduct("image_re/" + food.getPicPath() + ".jpg", food.getName(), priceNew,
//					food.getAmount());
			foodList.add(temp);
			maincor_panel.add(temp);
		}

		JPanel start_panel = new JPanel();
		start_panel.setBounds(0, 185, 624, 150);
		start_panel.setLayout(new GridLayout(1, 4));
		frame.getContentPane().add(start_panel);
		start_panel.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0), 1, true), "อาหารว่าง",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));

		for (int i = 0; i < StarterFood.size(); i++) {
			Food food = StarterFood.get(i);
			int priceNew = food.getPrice();
			
			try {
				nIcon = ImageIO.read(getClass().getResource("/images/"+food.getPicPath()+".jpg"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			CustomProduct temp = new CustomProduct(nIcon, food.getName(), priceNew,
					food.getAmount());
//			CustomProduct temp = new CustomProduct("image_re/" + food.getPicPath() + ".jpg", food.getName(), priceNew,
//					food.getAmount());
			foodList.add(temp);
			start_panel.add(temp);
		}

		JLabel lb_extra = new JLabel("หมายเหตุ");
		lb_extra.setBounds(10, 340, 56, 14);
		frame.getContentPane().add(lb_extra);

		JTextArea extra_text = new JTextArea();
		extra_text.setBounds(10, 360, 614, 26);
		frame.getContentPane().add(extra_text);

		JButton btn_confirm = new JButton("ยืนยัน");
		btn_confirm.setBounds(436, 390, 89, 23);
		frame.getContentPane().add(btn_confirm);
		btn_confirm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				ArrayList<String[]> arr = new ArrayList<>();
				int total = 0;
				for (int i = 0; i < foodList.size(); i++) {
					CustomProduct temp = foodList.get(i);
					String name = temp.getItemName();
					int amount = temp.getItemAmount();
					int price = temp.getItemPrice();
					int tol = amount * price;

					if (temp.getItemAmount() > 0) {
						String[] tempList = { name, amount + "", price + "" };
						arr.add(tempList);
						System.out.println("Summary of " + name + " = " + amount + " " + "total price = " + amount + "x"
								+ price + " = " + tol);
					}
					total += tol;
				}
				System.out.println("Total price is " + total + " baht.");

				String[][] x = new String[arr.size() + 2][3];
				x[0][0] = "Menu";
				x[0][1] = "Amount";
				x[0][2] = "price/dish";
				for (int i = 0; i < arr.size(); i++) {
					for (int j = 0; j < arr.get(i).length; j++) {
						x[i + 1][j] = arr.get(i)[j];
					}
				}
				x[arr.size() + 1][0] = "";
				x[arr.size() + 1][1] = "Total";
				x[arr.size() + 1][2] = "" + total;
				
				JTable table_summary = new JTable();
				table_summary.setBorder(new LineBorder(new Color(0, 0, 0)));
				table_summary.setModel(new DefaultTableModel(x, new String[] { "รายการอาหาร", "จำนวน", "ราคา" }));
				table_summary.getColumnModel().getColumn(0).setPreferredWidth(268);
				table_summary.getColumnModel().getColumn(1).setPreferredWidth(93);
				table_summary.getColumnModel().getColumn(2).setPreferredWidth(90);
				table_summary.setBounds(34, 69, 428, 179);
				// getContentPane().add(table_summary);
				System.out.println(total + "  Size");
				if (total != 0) {
					new SummaryPanel(frame, table_summary, user, customer, FoodChoosen);
					frame.setVisible(false);
				} else {
					JOptionPane.showMessageDialog(null, "Please choose at least one Food.\n", "Food is not selected",
							JOptionPane.WARNING_MESSAGE);
				}
				// new Summary(table_summary);

			}
		});

		JButton btn_cancle = new JButton("ยกเลิก");
		btn_cancle.setBounds(535, 390, 89, 23);
		frame.getContentPane().add(btn_cancle);
		btn_cancle.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				// new Menu();
				String sql = "DELETE FROM `customer` WHERE `customer`.`id` = '"+getTopID()+"'";
				db_connect.executeUpdata(sql);
				frame.dispose();
			}
		});

		frame.setVisible(true);

	}
	
	public int getTopID() {
		int nId = 0;
		db_connect = new DatabaseController();
		String sql = "SELECT * FROM customer ORDER BY id DESC LIMIT 0,1";
		PreparedStatement ps = db_connect.makePreparedStatement(sql);
		ResultSet resultSet = db_connect.executeSQL(ps);
		try {
			resultSet.next();
			nId = resultSet.getInt("id");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("nID: " + nId);
		return nId;
	}

}

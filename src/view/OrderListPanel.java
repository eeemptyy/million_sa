package view;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;

import controller.DatabaseController;
import controller.FileHandler;
import model.Customer;
import model.User;

public class OrderListPanel extends JFrame {

	private int id;
	private int foodOrder_id;
	private int total;
	private String status;
	private ResultSet resultSet;
	private PreparedStatement ps;
	private String sql;
	private JTable table;
	private Object[][] rowData;
	private Object[] columnNames = { "ลำดับ", "เลขที่ใบสั่ง", "ราคารวม", "สถานะ" };
	private DefaultTableModel tm;
	private Timer a;
	private DatabaseController db_connect;

	public OrderListPanel() {
		init();
	}

	public void init() {
		JPanel panel1 = new JPanel();
		setTitle("Million Steak Bar: OrderList");
		setBounds(350, 100, 550, 360);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout());

		JLabel lb_summary = new JLabel("รายการใบสั่งอาหาร");
		lb_summary.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lb_summary.setBounds(128, 11, 217, 47);
		getContentPane().add(lb_summary, BorderLayout.NORTH);

		JLabel lb_idtext = new JLabel("รายการลำดับที่");
		panel1.add(lb_idtext);
		rowData = getRowData();
		// columnNames = {"ลำดับ", "เลขที่ใบสั่ง", "ราคารวม", "สถานะ"};
		tm = new DefaultTableModel(rowData, columnNames);
		table = new JTable(tm);
		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane, BorderLayout.CENTER);

		JTextField orderId = new JTextField(5);
		orderId.setEditable(true);

		JComboBox<String> jb_status = new JComboBox<>();
		jb_status.addItem("working");
		jb_status.addItem("done");
		jb_status.addItem("cancel");
		jb_status.setEditable(false);

		JButton btn_submit = new JButton("ยืนยัน");
		btn_submit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String a = orderId.getText();
				String b = "";
				if (a.equals("")) {
					JOptionPane.showMessageDialog(null, "กรุณาระบุลำดับที่ต้องการแก้ไข\n", "ลำดับไม่ถูกต้อง",
							JOptionPane.ERROR_MESSAGE);
				} else if (Integer.parseInt(a) <= 0) {
					JOptionPane.showMessageDialog(null, "ลำดับต้องมีค่ามากกว่า 0 เท่านั้น\n", "ลำดับไม่ถูกต้อง",
							JOptionPane.ERROR_MESSAGE);
				} else {
					b = (String) jb_status.getSelectedItem();
					if (b.equals("cancel")){
						b = "c";
					}else if (b.equals("done")){
						b = "d";
					}else {
						b = "w";
					}
					DatabaseController db_connect = new DatabaseController();
					String sql = "SELECT * FROM `orderlist` WHERE id = '" + a + "'";
					PreparedStatement ps = db_connect.makePreparedStatement(sql);
					ResultSet resultSet = db_connect.executeSQL(ps);
					try {
						resultSet.next();
						String status = resultSet.getString("status");
						if (status.equals("w")) {
							sql = "UPDATE `orderlist` SET `status` = '" + b + "' WHERE `orderlist`.`id` = '" + a + "'";
							db_connect.executeUpdata(sql);
							deleyPane("แก้ไขสถานนะเสร็จสิ้น\nลำดับที่ " + a + " เปลี่ยนสถานะเป็น " + b,
									"แก้ไขเสร็จสมบูรณ์", JOptionPane.INFORMATION_MESSAGE, 1000);
							// JOptionPane.showMessageDialog(null,
							// "แก้ไขสถานนะเสร็จสิ้น\nลำดับที่ "+a+"
							// เปลี่ยนสถานะเป็น "+b, "แก้ไขเสร็จสมบูรณ์",
							// JOptionPane.INFORMATION_MESSAGE);
						} else {
							// deleyPane("รายการลำดับที่ "+a+"
							// ถูกยกเลิกเรียบร้อยแล้ว\n", "แก้ไขเสร็จสมบูรณ์",
							// JOptionPane.INFORMATION_MESSAGE, 1000);
							JOptionPane.showMessageDialog(null, "รายการลำดับที่ " + a + "ได้ทำงานเสร็จเรียบร้อยไปแล้ว\n",
									"แก้ไขเสร็จสมบูรณ์", JOptionPane.INFORMATION_MESSAGE);
						}
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

			}
		});

		JButton btn_print = new JButton("พิมพ์ใบเสร็จ");
		btn_print.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String dateNow = LocalDate.now()+"";
				String x = LocalTime.now() + "";
				String timeNow = "" + x.subSequence(0, x.length() - 4);
				
				
				String a = orderId.getText();
				String b = "";
				if (a.equals("")) {
					JOptionPane.showMessageDialog(null, "กรุณาระบุลำดับที่ต้องการพิมพ์\n", "ลำดับไม่ถูกต้อง",
							JOptionPane.ERROR_MESSAGE);
				} else if (Integer.parseInt(a) <= 0) {
					JOptionPane.showMessageDialog(null, "ลำดับต้องมีค่ามากกว่า 0 เท่านั้น\n", "ลำดับไม่ถูกต้อง",
							JOptionPane.ERROR_MESSAGE);
				} else {
					b = (String) jb_status.getSelectedItem();
					DatabaseController db_connect = new DatabaseController();
					String sql = "SELECT * FROM `orderlist` WHERE id = '" + a + "'";
					PreparedStatement ps = db_connect.makePreparedStatement(sql);
					ResultSet resultSet = db_connect.executeSQL(ps);
					try {
						resultSet.next();
						String status = resultSet.getString("status");
						if (!status.equals("d")) {
							JOptionPane.showMessageDialog(null, "ต้องเป็นรายการที่มีสถานะ done แล้วเท่านั้น \n",
									"สถานะไม่ถูกต้อง", JOptionPane.ERROR_MESSAGE);
						} else {
							deleyPane("กำลังพิมพ์ใบเสร็จ", "พิมพ์ใบเสร็จ", JOptionPane.INFORMATION_MESSAGE, 2300);
							insertReceiptDB(Integer.parseInt(a),dateNow, timeNow);
							
						}
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

			}
		});

		JButton btn_reload = new JButton("โหลดใหม่");
		btn_reload.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				table.setModel(new DefaultTableModel(getRowData(), columnNames));
				tm.fireTableStructureChanged();
			}
		});
		
		panel1.add(orderId);
		panel1.add(jb_status);
		panel1.add(btn_submit);
		panel1.add(btn_print);
		panel1.add(btn_reload);
		add(panel1, BorderLayout.SOUTH);
		setVisible(true);
	}

	public void insertReceiptDB(int id, String date, String time) {
		sql = "SELECT * FROM `orderlist` WHERE id = '" + id + "'";
		ps = db_connect.makePreparedStatement(sql);
		resultSet = db_connect.executeSQL(ps);
		int foodorder_id = 0;
		int totalAmount = 0;
		String username = "";
		int customer_id=0;
		int orderlist_id=id;
		int totalP=0;
		ArrayList<ArrayList> repData = new ArrayList<>();
		try {
			resultSet.next();
			foodorder_id = resultSet.getInt("foodorder_id");
			totalP = resultSet.getInt("total");
			sql = "SELECT * FROM `foodorder` WHERE id = '" + foodorder_id + "'";
			ps = db_connect.makePreparedStatement(sql);
			resultSet = db_connect.executeSQL(ps);
			resultSet.next();
			username = resultSet.getString("username");
			customer_id = resultSet.getInt("customer_id");
			totalAmount = resultSet.getInt("amount");
//			time = resultSet.getString("time");
//			date = resultSet.getString("date");
			while (resultSet.next()){
				ArrayList<String> temp = new ArrayList<>();
				String[] temp2 = getMenuDel(resultSet.getInt("menu_id")).split(" ");
				temp.add(temp2[0]);
				temp.add(resultSet.getInt("amount")+"");
				temp.add(temp2[1]);
				repData.add(temp);
				System.out.println(totalAmount + "Total Amount");
				totalAmount += resultSet.getInt("amount");
			}
		} catch (

		SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sql = "INSERT INTO `receipt` (`id`, `username`, `customer_id`, `orderlist_id`, "
				+ "`amount`, `total`, `time`, `date`) VALUES (NULL, '"+username+"', '"+customer_id+"',"
						+ " '"+orderlist_id+"', '"+totalAmount+"', '"+totalP+"', '"+time+"', '"+date+"')";
		db_connect.executeUpdata(sql);
		
		User user;
		Customer customer;
		sql = "select * from user where username = '"+username+"'";
		ps = db_connect.makePreparedStatement(sql);
		resultSet = db_connect.executeSQL(ps);
		try {
			resultSet.next();
			user = new User(resultSet.getString("username"),
				resultSet.getString("password"), resultSet.getString("name"), resultSet.getString("tel"),
				resultSet.getString("email"), resultSet.getString("position"));
			sql = "select * from customer where id = '"+customer_id+"'";
			ps = db_connect.makePreparedStatement(sql);
			resultSet = db_connect.executeSQL(ps);
			resultSet.next();
			customer = new Customer(resultSet.getString("name"), resultSet.getString("tel"), resultSet.getString("email"));
			customer.setId(resultSet.getInt("id"));
			FileHandler aa = new FileHandler();
			String temp = aa.genDataReceipt(repData, user, customer, date, time);
			aa.writeReceipt(temp, "testWriteTemp");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getMenuDel(int menu_id){
		sql = "SELECT * FROM `menu` INNER JOIN foodlist on menu.food_id = foodlist.id WHERE menu.id = '"+menu_id+"'";
//		sql = "select * from menu where id = '"+menu_id+"'";
		ps = db_connect.makePreparedStatement(sql);
		resultSet = db_connect.executeSQL(ps);
		String tName="";
		int tP = 0;
		try {
			while (resultSet.next()){
				tName = resultSet.getString("name");
				tP = resultSet.getInt("menu.price");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tName+" "+tP;
	}

	public void deleyPane(String text, String title, int type, int time) {
		JOptionPane pane = new JOptionPane(text, type);
		JDialog dialog = pane.createDialog(null, title);
		dialog.setModal(false);
		dialog.setVisible(true);

		ActionListener b = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.setVisible(false);
				System.out.println("XXX " + time);
				table.setModel(new DefaultTableModel(getRowData(), columnNames));
				tm.fireTableStructureChanged();
				a.stop();
			}
		};
		a = new Timer(time, b);
		a.start();

	}

	public Object[][] getRowData() {
		Object[][] rowdata = null;
		db_connect = new DatabaseController();
		sql = "SELECT * FROM `orderlist`";
		ps = db_connect.makePreparedStatement(sql);
		resultSet = db_connect.executeSQL(ps);
		ArrayList<ArrayList> list = new ArrayList<>();
		try {
			while (resultSet.next()) {
				id = resultSet.getInt("id");
				foodOrder_id = resultSet.getInt("foodorder_id");
				total = resultSet.getInt("total");
				status = resultSet.getString("status");
				ArrayList<String> temp = new ArrayList<>();
				temp.add(String.valueOf(id));
				temp.add(String.valueOf(foodOrder_id));
				temp.add(String.valueOf(total));
				temp.add(status);
				list.add(temp);
			}
			rowdata = new Object[list.size()][4];
			for (int j = 0; j < list.size(); j++) {
				Object[] tt = list.get(j).toArray();
				rowdata[j] = tt;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rowdata;
	}

	public int getTotalPrice(int foodOr_id) {
		System.out.println("INNN: " + foodOr_id);
		int total = 0;
		DatabaseController db_connect = new DatabaseController();
		String sql = "SELECT * FROM `foodorder` WHERE id = '" + foodOr_id + "'";
		PreparedStatement ps = db_connect.makePreparedStatement(sql);
		ResultSet resultSet = db_connect.executeSQL(ps);
		ArrayList<Integer> menuIdList = new ArrayList<>();
		ArrayList<Integer> amountList = new ArrayList<>();
		ArrayList<Integer> priceList = new ArrayList<>();
		try {
			while (resultSet.next()) {
				int memid = resultSet.getInt("menu_id");
				int amo = resultSet.getInt("amount");
				menuIdList.add(memid);
				amountList.add(amo);
				System.out.println("Memid: " + memid + " |amo: " + amo);
			}
			for (int i = 0; i < menuIdList.size(); i++) {
				sql = "SELECT * FROM `menu` WHERE id = '" + menuIdList.get(i) + "'";
				ps = db_connect.makePreparedStatement(sql);
				resultSet = db_connect.executeSQL(ps);
				while (resultSet.next()) {
					int pri = resultSet.getInt("price");
					priceList.add(pri);
					System.out.println("pri: " + pri);
				}
			}
			for (int i = 0; i < priceList.size(); i++) {
				total += (priceList.get(i) * amountList.get(i));
				System.out.println("Total " + total);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return total;
	}

	public int getTopID() {
		int nId = 0;
		DatabaseController db_connect = new DatabaseController();
		String sql = "SELECT * FROM customer ORDER BY id DESC LIMIT 0,1";
		PreparedStatement ps = db_connect.makePreparedStatement(sql);
		ResultSet resultSet = db_connect.executeSQL(ps);
		try {
			resultSet.next();
			nId = resultSet.getInt("id");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("nID: " + nId);
		return nId;
	}
}

package view;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalTime;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.Timer;

import controller.FileHandler;
import model.Customer;
import model.User;

public class PrintPOPanel extends JFrame {

	private JButton btn_print;
	private JLabel lb_email;
	private JLabel lb_date_order;
	private JLabel lb_time_order;
	private JTable table;
	private User user;
	private Customer customer;
	private String dateNow;
	private String timeNow;

	public PrintPOPanel(JTable table, User user, Customer customer) {
		this.table = table;
		this.user = user;
		this.customer = customer;
		init();
	}

	public void init() {

		setTitle("Million Steak Bar: PrintOrder");
		setBounds(350, 100, 500, 360);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);

		JLabel lb_PO = new JLabel("\u0E43\u0E1A\u0E2A\u0E31\u0E48\u0E07\u0E2D\u0E32\u0E2B\u0E32\u0E23");
		lb_PO.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lb_PO.setBounds(165, 11, 185, 47);
		getContentPane().add(lb_PO);

		table.getColumnModel().getColumn(0).setPreferredWidth(268);
		table.getColumnModel().getColumn(1).setPreferredWidth(93);
		table.getColumnModel().getColumn(2).setPreferredWidth(90);
		table.setBounds(34, 94, 428, 154);
		getContentPane().add(table);

		btn_print = new JButton("Print");
		btn_print.setBounds(181, 259, 89, 23);
		getContentPane().add(btn_print);
		btn_print.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane pane = new JOptionPane("กำลังพิมพ์ใบสั่งอาหาร", JOptionPane.INFORMATION_MESSAGE);
				JDialog dialog = pane.createDialog(null, "Million Steak Bar: Printing");
				dialog.setModal(false);
				dialog.setVisible(true);
				FileHandler aa = new FileHandler();
				String temp = aa.genDataPO(table, user, customer, dateNow, timeNow);
				aa.writePO(temp, "testWriteTemp");
				new Timer(2300, new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						dialog.setVisible(false);
						dispose();
					}
				}).start();
				
			}
		});

		lb_email = new JLabel("E-mail: " + customer.getEmail());
		lb_email.setBounds(34, 69, 149, 14);
		getContentPane().add(lb_email);
		dateNow = LocalDate.now()+"";
		lb_date_order = new JLabel(
				"\u0E2A\u0E31\u0E48\u0E07\u0E2D\u0E32\u0E2B\u0E32\u0E23\u0E27\u0E31\u0E19\u0E17\u0E35\u0E48 "
						+ LocalDate.now());
		lb_date_order.setBounds(193, 69, 149, 14);
		getContentPane().add(lb_date_order);

		String a = LocalTime.now() + "";
		timeNow = "" + a.subSequence(0, a.length() - 4);
		lb_time_order = new JLabel("\u0E40\u0E27\u0E25\u0E32 " + timeNow);
		lb_time_order.setBounds(352, 69, 110, 14);
		getContentPane().add(lb_time_order);

		setVisible(true);

	}
}

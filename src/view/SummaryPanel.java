package view;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;

import controller.DatabaseController;
import model.Customer;
import model.Food;
import model.User;

public class SummaryPanel extends JFrame {

	private JButton btn_confirm;
	private JButton btn_edit;
	private JFrame menu;
	private JTable table;
	private User user;
	private Customer customer;
	private ArrayList<Food> foodList = new ArrayList<>();
	private ArrayList<String> foodName = new ArrayList<>();
	private ArrayList<Integer> foodAmount = new ArrayList<>();

	public SummaryPanel(JFrame menu, JTable table, User user, Customer customer, ArrayList<Food> foodList) {
		this.menu = menu;
		this.table = table;
		this.user = user;
		this.customer = customer;
		this.foodList = foodList;
		init();
	}

	public SummaryPanel(JTable table) {
		this.table = table;
		init();
	}

	public void init() {

		setTitle("Million Steak Bar: Summary");
		setBounds(350, 100, 500, 360);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);

		JLabel lb_summary = new JLabel("สรุปเมนูอาหาร");
		lb_summary.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lb_summary.setBounds(128, 11, 217, 47);
		getContentPane().add(lb_summary);
		getContentPane().add(table);

		btn_confirm = new JButton("ยืนยัน");
		btn_confirm.setBounds(144, 259, 89, 23);
		getContentPane().add(btn_confirm);
		btn_confirm.addActionListener(new ActionListener() {

			private DatabaseController db_connect;

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				for (int i = 1; i < table.getRowCount() - 1; i++) {
					foodName.add((String) table.getValueAt(i, 0));
					foodAmount.add(Integer.parseInt((String) table.getValueAt(i, 1)));
					for (int j = 0; j < table.getColumnCount(); j++) {
						String ss = (String) table.getValueAt(i, j);
						System.out.println("This is value at i = " + i + " | j = " + j + " >> " + ss);
					}
				}
				String a = LocalTime.now() + "";
				String timeNow = "" + a.subSequence(0, a.length() - 4);
				db_connect = new DatabaseController();
				String sql = null;// "select * from menu";
				PreparedStatement ps = null;// db_connect.makePreparedStatement(sql);
				ResultSet result = null;// db_connect.executeSQL(ps);
				int menu_id = 0;

				int foodOrder_id = getTopID();
				for (int i = 0; i < foodList.size(); i++) {
					Food food = foodList.get(i);
					String name = food.getName();
					int food_id = food.getId();
					for (int j = 0; j < foodName.size(); j++) {
						if (foodName.get(j).equals(name)) {
							sql = "SELECT * FROM `menu` WHERE food_id = '" + food_id + "'";
							ps = db_connect.makePreparedStatement(sql);
							result = db_connect.executeSQL(ps);
							try {
								result.next();
								menu_id = result.getInt("id");
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							sql = "INSERT INTO foodorder (`id`, `username`, `customer_id`, "
									+ "`menu_id`, `amount`, `time`, `date`) " + "VALUES ('" + foodOrder_id + "', '"
									+ user.getUsername() + "', '" + customer.getId() + "', '" + menu_id + "'," + " '"
									+ foodAmount.get(j) + "', '" + timeNow + "', '" + LocalDate.now() + "')";
							db_connect.executeUpdata(sql);
						}
					}
				}
				int total = getTotalPrice(foodOrder_id);
				sql = "INSERT INTO `orderlist` (`id`, `foodorder_id`, `total`, `status`) VALUES (NULL, '" + foodOrder_id
						+ "', '" + total + "', 'w')";
				db_connect.executeUpdata(sql);
				new PrintPOPanel(table, user, customer);
				menu.dispose();
				dispose();
			}
		});

		btn_edit = new JButton("แก้ไข");
		btn_edit.setBounds(243, 259, 89, 23);
		getContentPane().add(btn_edit);
		btn_edit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				menu.setVisible(true);
				dispose();
			}
		});

		setVisible(true);
	}

	public int getTotalPrice(int foodOr_id) {
		System.out.println("INNN: " + foodOr_id);
		int total = 0;
		DatabaseController db_connect = new DatabaseController();
		String sql = "SELECT * FROM `foodorder` WHERE id = '" + foodOr_id + "'";
		PreparedStatement ps = db_connect.makePreparedStatement(sql);
		ResultSet resultSet = db_connect.executeSQL(ps);
		ArrayList<Integer> menuIdList = new ArrayList<>();
		ArrayList<Integer> amountList = new ArrayList<>();
		ArrayList<Integer> priceList = new ArrayList<>();
		try {
			while (resultSet.next()) {
				int memid = resultSet.getInt("menu_id");
				int amo = resultSet.getInt("amount");
				menuIdList.add(memid);
				amountList.add(amo);
				System.out.println("Memid: " + memid + " |amo: " + amo);
			}
			for (int i = 0; i < menuIdList.size(); i++) {
				sql = "SELECT * FROM `menu` WHERE id = '" + menuIdList.get(i) + "'";
				ps = db_connect.makePreparedStatement(sql);
				resultSet = db_connect.executeSQL(ps);
				while (resultSet.next()) {
					int pri = resultSet.getInt("price");
					priceList.add(pri);
					System.out.println("pri: " + pri);
				}
			}
			for (int i = 0; i < priceList.size(); i++) {
				total += (priceList.get(i) * amountList.get(i));
				System.out.println("Total " + total);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return total;
	}

	public int getTopID() {
		int nId = 0;
		DatabaseController db_connect = new DatabaseController();
		String sql = "SELECT * FROM customer ORDER BY id DESC LIMIT 0,1";
		PreparedStatement ps = db_connect.makePreparedStatement(sql);
		ResultSet resultSet = db_connect.executeSQL(ps);
		try {
			resultSet.next();
			nId = resultSet.getInt("id");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("nID: " + nId);
		return nId;
	}
}
